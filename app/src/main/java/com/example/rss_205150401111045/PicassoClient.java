package com.example.rss_205150401111045;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class PicassoClient {

    public static void downloadImage(Context c, String imageUrl, ImageView img){
        if (imageUrl!=null && imageUrl.length()>0) {
            Picasso.get().load(imageUrl).placeholder(R.drawable.ic_launcher_background).into(img);
        }
    }
}